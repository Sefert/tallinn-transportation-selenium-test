import static org.junit.jupiter.api.Assertions.assertTrue;

import Enums.DriverType;
import Factory.DriverFactory;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;


public class BaseTest {

    @Test
    public void HelloWorld(){
        WebDriver driver = DriverFactory.getDriver(DriverType.CHROME);
        driver.get("https://www.google.com");

        assertTrue(driver.getCurrentUrl().trim().equals("https://www.google.com/"));
    }
}
