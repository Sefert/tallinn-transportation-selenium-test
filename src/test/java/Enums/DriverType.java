package Enums;

public enum DriverType {
    CHROME,
    FIREFOX,
    EDGE,
    IE;
}
