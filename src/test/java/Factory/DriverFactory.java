package Factory;

import Enums.DriverType;
import Util.PropertyLoader;
import jdk.nashorn.internal.objects.annotations.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;


/*
https://www.vinsguru.com/selenium-webdriver-factory-design-pattern-using-java-8-supplier/
 */
public class DriverFactory {

    private static final Map<DriverType, Supplier<WebDriver>> driverMap = new HashMap<>();

    public static Properties properties;

    //chrome driver supplier
    private static final Supplier<WebDriver> chromeDriverSupplier = () -> {
        System.setProperty("webdriver.chrome.driver", properties.getProperty("chromeDriver"));
        return new ChromeDriver();
    };

    //firefox driver supplier
    private static final Supplier<WebDriver> firefoxDriverSupplier = () -> {
        System.setProperty("webdriver.gecko.driver", properties.getProperty("firefoxDriver"));
        return new FirefoxDriver();
    };

    //add all the drivers into a map
    static{
        //TODO: init properties elsewhere
        properties = PropertyLoader.loadApplicationProperties();

        driverMap.put(DriverType.CHROME, chromeDriverSupplier);
        driverMap.put(DriverType.FIREFOX, firefoxDriverSupplier);
    }

    public static WebDriver getDriver(DriverType type){
        return driverMap.get(type).get();
    }
}
