package Page.Header;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header {

    private WebDriver driver;

    public Header(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
        language = new Language(driver);
    }

    @FindBy(id="header2")
    WebElement title;

    @FindBy(className="mobile-link")
    WebElement mobileSwitch;

    @Getter Language language;
}
