package Page.Header;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Language {

    private WebDriver driver;

    public Language(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "a[title='Eesti keel']")
    WebElement estonian;
    @FindBy(css = "a[title='In English']")
    WebElement english;
    @FindBy(css = "a[title*='Suomen']")
    WebElement finnish;
    @FindBy(css = "a[title='На русском']")
    WebElement russian;
    @FindBy(css = "a[title='Auf Deutsch']")
    WebElement german;
    @FindBy(css = "a[title*='Lietuv']")
    WebElement lithuanian;
    @FindBy(css = "a[title*='Latviski']")
    WebElement latvian;
}
