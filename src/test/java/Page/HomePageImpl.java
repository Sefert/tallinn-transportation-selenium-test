package Page;

import Factory.DriverFactory;
import Page.NavBar.NavBarImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageImpl extends NavBarImpl {

    private static String url = DriverFactory.properties.getProperty("homePage");

    WebDriver driver;

    @FindBy(xpath=".//*[@id='login1']")
    WebElement username;

    public HomePageImpl(WebDriver driver) {
        super(driver);
    }
}
