package Page.NavBar;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AreaMenuImpl {

    private WebDriver driver;

    public AreaMenuImpl(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css="#areaMenu>a")
    WebElement schedule;
    @FindBy(id="tallinna-linn_bus")
    WebElement tallinnBus;
    @FindBy(id="tallinna-linn_tram")
    WebElement tallinnTram;
    @FindBy(id="tallinna-linn_trol")
    WebElement tallinnTrolley;
    @FindBy(id="schoolbus")
    WebElement schoolBus;
    @FindBy(id="ship")
    WebElement ship;
    @FindBy(id="region")
    WebElement region;
    @FindBy(id="harju_regionalbus")
    WebElement harjuRegionalBus;
    @FindBy(id="harju_commercialbus")
    WebElement harjuCommercialBus;
    @FindBy(id="harju_harju_train")
    WebElement harjuTrain;
}
