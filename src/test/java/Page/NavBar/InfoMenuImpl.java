package Page.NavBar;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMenuImpl {

    WebDriver driver;

    public InfoMenuImpl(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id="urlTicketsTallinn")
    WebElement urlTicketsTallinn;
    @FindBy(id="urlTicketsHarjumaa")
    WebElement urlTicketsHarjumaa;
    @FindBy(id="urlNewsTallinn")
    WebElement urlNewsTallinn;
    @FindBy(id="urlNewsHarjumaa")
    WebElement urlNewsHarjumaa;
}
