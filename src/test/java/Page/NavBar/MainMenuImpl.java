package Page.NavBar;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainMenuImpl {

    WebDriver driver;

    public MainMenuImpl(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id="menuPlan")
    WebElement planner;
    @FindBy(id="menuMap")
    WebElement map;
    @FindBy(id="menuFavourites")
    WebElement favourites;
    @FindBy(id="menuContacts")
    WebElement contacts;
}
