package Page.NavBar;

import Factory.DriverFactory;
import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class NavBarImpl {

    public NavBarImpl(WebDriver driver) {
        this.areaMenu = new AreaMenuImpl(driver);
        this.infoMenu = new InfoMenuImpl(driver);
        this.mainMenu = new MainMenuImpl(driver);
    }

    AreaMenuImpl areaMenu;
    InfoMenuImpl infoMenu;
    MainMenuImpl mainMenu;
}
