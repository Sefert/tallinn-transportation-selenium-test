package Page.TravelPlanner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TravelPlanner {

    WebDriver driver;

    public TravelPlanner(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "inputStart")
    WebElement startPoint;
    @FindBy(id = "inputFinish")
    WebElement endPoint;

    @FindBy(className = "switch")
    WebElement switchAfter;

    @FindBy(id = "inputReverseDepart")
    WebElement inputReverseDepart;
    @FindBy(id = "inputReverseArrive")
    WebElement inputReverseArrive;
    @FindBy(id = "inputTime")
    WebElement inputTime;
    @FindBy(id = "inputDateNew")
    WebElement inputDateNew;


    @FindBy(className= "advanced")
    WebElement advancedOptionsSetting;
    @FindBy(id = "advancedOptions")
    WebElement advancedOptionsButton;

    private TravelPlannerAdvanced advancedOptions;

    public TravelPlannerAdvanced getAdvancedOptions(){
        if (advancedOptionsActive()){
            advancedOptionsButton.click();
        }
        advancedOptions = new TravelPlannerAdvanced(this.driver);
        return advancedOptions;
    }

    public boolean advancedOptionsActive(){
        return !advancedOptionsSetting.getAttribute("style").contains("none");
    }
}