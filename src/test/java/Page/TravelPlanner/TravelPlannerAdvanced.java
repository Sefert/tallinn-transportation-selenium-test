package Page.TravelPlanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class TravelPlannerAdvanced {

    WebDriver driver;

    public TravelPlannerAdvanced(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /*
    * Handicapped section
    */
    @FindBy(id = "checkHandicapped")
    WebElement checkHandicapped;
    @FindBy(id = "labelHandicapped")
    WebElement labelHandicapped;

    /*
     * Transportation checkbox section
     */
    @FindBy(id = "uniform-checkboxbus")
    WebElement checkBoxBus;
    @FindBy(id = "uniform-checkboxtrol")
    WebElement checkBoxTrol;
    @FindBy(id = "uniform-checkboxtram")
    WebElement checkBoxTram;
    @FindBy(id = "uniform-checkboxregionalbus")
    WebElement checkBoxRegionalBus;
    @FindBy(id = "uniform-checkboxcommercialbus")
    WebElement getCheckBoxCommercialBus;
    @FindBy(id = "uniform-checkboxtrain")
    WebElement checkBoxTrain;

    /*
     * Personal filters section
     */
    @FindBy(id = "labelRoutes")
    WebElement transportationLabel;
    @FindBy(id = "inputRoutesFilter")
    WebElement inputTransportationNumber;

    @FindBy(id = "labelChangeTimeText")
    WebElement labelChangeTimeText;
    @FindBy(id = "inputChangeTime")
    Select inputChangeTime;

    @FindBy(id = "labelWalkMaxText")
    WebElement labelWalkMaxText;
    @FindBy(id = "inputWalkMax")
    Select inputWalkMax;

    @FindBy(id = "labelWalkSpeedText")
    WebElement labelWalkSpeedText;
    @FindBy(id = "inputWalkSpeed")
    Select inputWalkSpeed;

    @FindBy(css = ".buttons>a")
    WebElement searchButton;

    @FindBy(id = "footer-info")
    WebElement footer;

    /*
     * Helper for transportation checkbox section
     */
    public void clickTransportCheckBox(WebElement ... transport){
        for (WebElement element : transport){
            element.findElement(By.cssSelector("span>input")).click();
        }
    }

    public String getText(WebElement transport){
        return  transport.findElement(By.cssSelector("label")).getText();
    }

    public List<WebElement> getFooterRows(){
        return footer.findElements(By.cssSelector("p"));
    }

    public List<WebElement> getFooterRowLink(WebElement element){
        return element.findElements(By.cssSelector("a"));
    }
}
